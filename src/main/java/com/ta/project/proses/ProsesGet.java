package com.ta.project.proses;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.fasterxml.jackson.databind.ObjectMapper;


public class ProsesGet implements Processor{
	Logger log = Logger.getLogger("ProsesGet");
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String valueGet = getApi();
		log.info("Value Get = " +valueGet);
		
		exchange.getOut().setBody(valueGet);
	}
	
	public String getApi () {
		String responseApi = "";
		try {

            URL url = new URL("https://api.coindesk.com/v1/bpi/currentprice.json");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
          
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                responseApi = output;
            }
            conn.disconnect();
            
           
        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
            
        }
		return responseApi;
	}
}
