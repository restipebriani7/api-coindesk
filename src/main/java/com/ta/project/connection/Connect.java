package com.ta.project.connection;

import java.sql.Connection;

import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Connect {
	static Logger log = LoggerFactory.getLogger(Connect.class);
	public static Connection conn;
	public static Connection connection() {
		try {
			PGSimpleDataSource source = new PGSimpleDataSource();
			
//			source.setDatabaseName(parameterDB.getDb());
//			source.setServerName(parameterDB.getEndPoint());
//			source.setPortNumber(Integer.parseInt(parameterDB.getPort()));
//			source.setUser(parameterDB.getUsername());
//			source.setPassword(parameterDB.getPassword());

//			source.setServerName("localhost");
//			source.setPortNumber(5432);
//			source.setDatabaseName("postgres");
//			source.setPassword("12345678");
//			source.setUser("postgres");
			
			conn = source.getConnection();
			
			log.info("Connect is Success");
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Connect is Failed");
			log.info("EXCEPTION : " +e.getMessage());
		}		
		return conn;
	}
}
